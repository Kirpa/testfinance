//
//  AppDelegate.h
//  TestFinance
//
//  Created by Vadim Zhepetov on 29/06/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

