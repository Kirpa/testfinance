//
//  BarChartViewWithLines.h
//  TestFinance
//
//  Created by Vadim Zhepetov on 02/07/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import "JBBarChartView.h"

@interface BarChartViewWithLines : JBBarChartView

@property (nonatomic) NSInteger verticalGuideInterval;
@property (nonatomic) NSInteger horizontalGuideInterval;

@end
