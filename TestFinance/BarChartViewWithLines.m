//
//  BarChartViewWithLines.m
//  TestFinance
//
//  Created by Vadim Zhepetov on 02/07/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import "BarChartViewWithLines.h"

@interface BarChartViewWithLines()

@property (nonatomic) NSMutableArray *guides;

@end

@implementation BarChartViewWithLines

- (NSMutableArray *)guides
{
    if (!_guides){
        _guides = [NSMutableArray array];
    }

    return _guides;
}

- (UIView *)guidePrototype
{
    UIView *result = [[UIView alloc] init]; // We're not redrawing this chart, hence just create view wothout caching
    result.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1];

    return result;
}

- (UIView *)createHorizontalGuide
{
    UIView *result = [self guidePrototype];
    result.bounds = CGRectMake(0, 0, self.bounds.size.width, 1);

    return result;
}

- (UIView *)createVerticalGuide
{
    UIView *result = [self guidePrototype];
    result.bounds = CGRectMake(0, 0, 1, self.bounds.size.height);

    return result;
}

- (void)removeAllGuides
{
    [self.guides makeObjectsPerformSelector:@selector(removeFromSuperview) withObject:nil];
    [self.guides removeAllObjects];
}

- (void)drawHorizontalLines
{
    CGFloat maxValue = [self maximumValue];
    CGFloat viewHeight = self.bounds.size.height;
    CGFloat pointStep = (viewHeight - self.headerPadding) / maxValue * self.horizontalGuideInterval;
    CGFloat currentY = pointStep;
    UIView *targetBar = [self barViewAtIndex:0];
    while (currentY < viewHeight){
        currentY = ceil(currentY);
        UIView *guide = [self createHorizontalGuide];
        guide.center = CGPointMake(CGRectGetMidX(self.bounds), currentY);
        [self insertSubview:guide belowSubview:targetBar];
        [self.guides addObject:guide];
        currentY += pointStep;
    }
}

- (void)drawVerticalLines
{
    NSUInteger barCount = [self.dataSource numberOfBarsInBarChartView:self];
    for (int i = 1; i < barCount; i++){
        if (i % (self.verticalGuideInterval) == 0){
            UIView *targetBar = [self barViewAtIndex:i];
            UIView *guide = [self createVerticalGuide];
            guide.center = CGPointMake(CGRectGetMaxX(targetBar.frame), CGRectGetMidY(self.bounds));
            [self insertSubview:guide belowSubview:targetBar];
            [self.guides addObject:guide];
        }
    }
}

- (void)drawGuides
{
    [self drawHorizontalLines];
    [self drawVerticalLines];

}

- (void)reloadData
{
    [super reloadData];
    [self removeAllGuides];
    [self drawGuides];
}

@end
