//
//  ChartScreenView.h
//  TestFinance
//
//  Created by Vadim Zhepetov on 01/07/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BarChartViewWithLines;

@interface ChartScreenView : UIView

@property (nonatomic) IBOutlet BarChartViewWithLines *chartView;

@end
