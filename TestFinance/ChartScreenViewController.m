//
//  ViewController.m
//  TestFinance
//
//  Created by Vadim Zhepetov on 29/06/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import "ChartScreenViewController.h"
#import "ChartScreenView.h"
#import "RateProvider.h"
#import "BarChartViewWithLines.h"
#import "RateDailyData.h"
#import "RateDataHistory.h"

@interface ChartScreenViewController () <JBBarChartViewDataSource, JBBarChartViewDelegate>

@property (nonatomic) IBOutlet ChartScreenView *portraitView;
@property (nonatomic) IBOutlet ChartScreenView *landscapeView;
@property (nonatomic) ChartScreenView *activeView;
@property (nonatomic) UIView *rotationSnapshotView;
@property (nonatomic) RateProvider *rateProvider;
@property (nonatomic) RateDataHistory *fetchedData;
@property (nonatomic) UIColor *risingColor;
@property (nonatomic) UIColor *fallingColor;

@end

@implementation ChartScreenViewController

#pragma mark - Chart delegate

- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
    return [self.fetchedData daysCount];
}

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    RateDailyData *data = [self.fetchedData dailyDataAtIndex:index];
    return [data.rate floatValue];
}

- (UIColor *)barChartView:(JBBarChartView *)barChartView colorForBarViewAtIndex:(NSUInteger)index
{
    if ([self.fetchedData rateIsRisingAtIndex:index])
        return self.risingColor;
    else
        return self.fallingColor;
}

#pragma mark - Helpers

- (void)configureChartView:(BarChartViewWithLines *)chartView
{
    chartView.dataSource = self;
    chartView.delegate = self;
    chartView.headerPadding = 20;
    chartView.verticalGuideInterval = 7;  // Condition for vertical lines not specified, let it be 7
    chartView.horizontalGuideInterval = 10; // Draw line for every 10 RUB
}

#pragma mark - Data 

- (void)queryData
{
    __weak __typeof(self) weakSelf = self;
    [self.rateProvider updateDataWithSucces:^(RateDataHistory *data){
        NSLog(@"Api call success");
        [weakSelf dataFetched:data];
    } failure:^(NSError *error){
        NSLog(@"Api call error: %@", error);
    }];
}

- (void)dataFetched:(RateDataHistory *)data
{
    self.fetchedData = data;
    [self updateChart];
}

- (void)updateChart
{
    self.activeView.chartView.minimumValue = [[self.fetchedData lowestRate] floatValue]  * 0.98;
    [self.activeView.chartView reloadData];
}

#pragma mark - Orientation handling

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.portraitView.frame = self.view.bounds;
    self.landscapeView.frame = self.view.bounds;
}

- (UIView*)viewForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation)){
        if (self.landscapeView == nil){
            UINib *landscapeViewXib = [UINib nibWithNibName:@"ChartScreenViewLandscape" bundle:nil];
            [landscapeViewXib instantiateWithOwner:self options:nil];
            [self configureChartView:self.landscapeView.chartView];
        }
        self.activeView = self.landscapeView;
    } else {
        if (self.portraitView == nil){
            UINib *portraitViewXib = [UINib nibWithNibName:@"ChartScreenViewPortrait" bundle:nil];
            [portraitViewXib instantiateWithOwner:self options:nil];
            [self configureChartView:self.portraitView.chartView];
        }
        self.activeView = self.portraitView;
    }

    return self.activeView;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];

    UIView *viewForCurrentInterfaceOrientation = [self viewForInterfaceOrientation:self.interfaceOrientation];
    UIView *viewForFinalInterfaceOrientation = [self viewForInterfaceOrientation:toInterfaceOrientation];

    if (viewForCurrentInterfaceOrientation != viewForFinalInterfaceOrientation)
    {
        self.rotationSnapshotView = [viewForCurrentInterfaceOrientation snapshotViewAfterScreenUpdates:NO];
        self.rotationSnapshotView.frame = viewForCurrentInterfaceOrientation.frame;
        self.rotationSnapshotView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view insertSubview:self.rotationSnapshotView aboveSubview:viewForCurrentInterfaceOrientation];

        [viewForCurrentInterfaceOrientation removeFromSuperview];
    }

    if (viewForFinalInterfaceOrientation.superview == nil)
    {
        [self.view addSubview:viewForFinalInterfaceOrientation];
        [self.view sendSubviewToBack:viewForFinalInterfaceOrientation];
        viewForFinalInterfaceOrientation.alpha = 0.0f;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    self.rotationSnapshotView.alpha = 0.0f;
    [self viewForInterfaceOrientation:toInterfaceOrientation].alpha = 1.0f;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.rotationSnapshotView removeFromSuperview];
    self.rotationSnapshotView = nil;
    [self updateChart];
}

#pragma mark - Object lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.rateProvider = [[RateProvider alloc] init];

    self.risingColor = [UIColor colorWithRed:0.38 green:0.98 blue:0.65 alpha:1];
    self.fallingColor = [UIColor colorWithRed:1 green:0.33 blue:0.35 alpha:1];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self queryData];

    UIView *viewForCurrentInterfaceOrientation = [self viewForInterfaceOrientation:self.interfaceOrientation];
    if (viewForCurrentInterfaceOrientation.superview == nil){
        [self.portraitView removeFromSuperview];
        [self.landscapeView removeFromSuperview];

        [self.view addSubview:viewForCurrentInterfaceOrientation];
        [self.view sendSubviewToBack:viewForCurrentInterfaceOrientation];
    }
}

- (void)loadView
{
    self.view = [[UIView alloc] init];
}

@end
