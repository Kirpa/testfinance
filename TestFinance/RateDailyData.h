//
//  DailyData.h
//  TestFinance
//
//  Created by Vadim Zhepetov on 30/06/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RateDailyData : NSObject

@property (nonatomic) NSDate *date;
@property (nonatomic) NSNumber *rate;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
