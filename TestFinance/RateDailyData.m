//
//  DailyData.m
//  TestFinance
//
//  Created by Vadim Zhepetov on 30/06/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import "RateDailyData.h"

static NSString *const kDateKey = @"Date";
static NSString *const kRateKey = @"Close";

@implementation RateDailyData

#pragma mark - Helpers

- (NSDateFormatter *)responseDateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
    });
    return dateFormatter;
}

- (NSNumberFormatter *)responseNumberFormatter
{
    static NSNumberFormatter *numberFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        numberFormatter = [[NSNumberFormatter alloc] init];
    });

    return numberFormatter;
}

#pragma mark - Object lifecycle

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init])){
        NSDateFormatter *dateFormatter = [self responseDateFormatter];
        self.date = [dateFormatter dateFromString:dictionary[kDateKey]];
        NSNumberFormatter *numberFormatter = [self responseNumberFormatter];
        self.rate = [numberFormatter numberFromString:dictionary[kRateKey]];
    }

    return self;
}

@end
