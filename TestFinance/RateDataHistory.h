//
//  RateDataHistory.h
//  TestFinance
//
//  Created by Vadim Zhepetov on 01/07/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RateDailyData;

@interface RateDataHistory : NSObject

@property (nonatomic, readonly) NSNumber *highestRate;
@property (nonatomic, readonly) NSNumber *lowestRate;

- (void)addDailyData:(RateDailyData *)data;
- (void)sortByDate;
- (RateDailyData *)dailyDataAtIndex:(NSInteger)index;
- (BOOL)rateIsRisingAtIndex:(NSInteger)index;
- (NSInteger)daysCount;

@end
