//
//  RateDataHistory.m
//  TestFinance
//
//  Created by Vadim Zhepetov on 01/07/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import "RateDataHistory.h"
#import "RateDailyData.h"

@interface RateDataHistory()

@property (nonatomic) NSMutableArray *data;
@property (nonatomic, readwrite) NSNumber *highestRate;
@property (nonatomic, readwrite) NSNumber *lowestRate;

@end

@implementation RateDataHistory

#pragma mark - Public methods

- (BOOL)rateIsRisingAtIndex:(NSInteger)index
{
    if (index == 0)
        return YES;

    RateDailyData *currentData = self.data[index];
    RateDailyData *previousData = self.data[index - 1];

    NSComparisonResult comparison = [previousData.rate compare:currentData.rate];
    return comparison != NSOrderedDescending;
}

- (void)addDailyData:(RateDailyData *)data
{
    [self.data addObject:data];
    if ([self.highestRate compare:data.rate] == NSOrderedAscending)
        self.highestRate = data.rate;

    if ([self.lowestRate compare:data.rate] == NSOrderedDescending)
        self.lowestRate = data.rate;
}

- (void)sortByDate
{
    [self.data sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *date1 = [(RateDailyData *)obj1 date];
        NSDate *date2 = [(RateDailyData *)obj2 date];
        return [date1 compare:date2];
    }];
}

- (RateDailyData *)dailyDataAtIndex:(NSInteger)index
{
    return self.data[index];
}

- (NSInteger)daysCount
{
    return self.data.count;
}

#pragma mark - Object lifecycle

- (instancetype)init
{
    if ((self = [super init])){
        self.data = [NSMutableArray array];
        self.highestRate = @0;
        self.lowestRate = @MAXFLOAT;
    }

    return self;
}

@end
