//
//  RateProvides.h
//  TestFinance
//
//  Created by Vadim Zhepetov on 30/06/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RateDataHistory;

@interface RateProvider : NSObject

- (void)updateDataWithSucces:(void (^)(RateDataHistory *))successBlock failure:(void (^)(NSError *))errorBlock;

@end
