//
//  RateProvides.m
//  TestFinance
//
//  Created by Vadim Zhepetov on 30/06/15.
//  Copyright (c) 2015 none. All rights reserved.
//

#import "RateProvider.h"
#import "AFNetworking.h"
#import "RateDailyData.h"
#import "RateDataHistory.h"

static NSString *const kApiCallString = @"https://query.yahooapis.com/v1/public/yql";
static NSString *const kQueryFormat = @"q=select * from yahoo.finance.historicaldata where symbol = \"RUB=X\" and startDate = \"%@\" and endDate = \"%@\"&format=json&env=store://datatables.org/alltableswithkeys";
static NSString *const kResponseQuoteDataKeypath = @"query.results.quote";

@implementation RateProvider

#pragma mark - Public methods

- (void)updateDataWithSucces:(void (^)(RateDataHistory *))successBlock failure:(void (^)(NSError *))errorBlock
{
    __weak __typeof(self) weakSelf = self;
    [self queryYahooWithSucces:^(NSArray *responseData){
        RateDataHistory *dailyDataArray = [weakSelf dailyDataFromResponse:responseData];
        successBlock(dailyDataArray);        
    } failure:errorBlock];
}

#pragma mark - Yahoo response consuming

- (RateDataHistory *)dailyDataFromResponse:(NSArray *)responseData
{
    RateDataHistory *result = [[RateDataHistory alloc] init];
    for (NSDictionary *oneDayDescription in responseData){
        RateDailyData *dailyData = [[RateDailyData alloc] initWithDictionary:oneDayDescription];
        [result addDailyData:dailyData];
    }

    [result sortByDate];
    return result;
}

#pragma mark - Yahoo API query

- (NSString *)formatDateForYahooQuery:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    return [dateFormatter stringFromDate:date];
}

- (NSDate *)previousMonthForDate:(NSDate *)date
{
    NSDateComponents *dateOffset = [[NSDateComponents alloc] init];
    dateOffset.month = -1;
    dateOffset.day = -1;
    NSCalendar *gregorian = [NSCalendar calendarWithIdentifier:@"gregorian"];
    return [gregorian dateByAddingComponents:dateOffset toDate:date options:0];
}

- (NSURL *)yahooQueryURL
{
    NSDate *now = [NSDate date];
    NSString *todayString = [self formatDateForYahooQuery:now];
    NSDate *previousMonth = [self previousMonthForDate:now];
    NSString *previousMonthString = [self formatDateForYahooQuery:previousMonth];

    NSString *query = [NSString stringWithFormat:kQueryFormat, previousMonthString, todayString];
    NSString *urlString = [NSString stringWithFormat:@"%@?%@", kApiCallString, [query stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];

    return [NSURL URLWithString:urlString];
}

- (void)queryYahooWithSucces:(void (^)(NSArray *data))successBlock failure:(void (^)(NSError *))errorBlock
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[self yahooQueryURL]];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock([responseObject valueForKeyPath:kResponseQuoteDataKeypath]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorBlock) {
            errorBlock(error);
        };
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
}

@end
